#!/bin/bash
# Clear the screen
clear


procs_1=80
procs_2=72
procs_3=64
procs_4=88
procs_5=96
procs_6=112
procs_7=58



h2_1=10
h2_2=50
h2_3=100
h2_4=150
h2_5=200
h2_6=250
h2_7=300
h2_8=350
h2_9=400
h2_10=450 



mpiexec -n $procs_1 ./allreduce_ring_async $h2_1
mpiexec -n $procs_1 ./allreduce_ring $h2_1
mpiexec -n $procs_1 ./allreduce $h2_1

mpiexec -n $procs_1 ./allreduce_ring_async $h2_2
mpiexec -n $procs_1 ./allreduce_ring $h2_2
mpiexec -n $procs_1 ./allreduce $h2_2

mpiexec -n $procs_1 ./allreduce_ring_async $h2_3
mpiexec -n $procs_1 ./allreduce_ring $h2_3
mpiexec -n $procs_1 ./allreduce $h2_3

mpiexec -n $procs_1 ./allreduce_ring_async $h2_4
mpiexec -n $procs_1 ./allreduce_ring $h2_4
mpiexec -n $procs_1 ./allreduce $h2_4

mpiexec -n $procs_1 ./allreduce_ring_async $h2_5
mpiexec -n $procs_1 ./allreduce_ring $h2_5
mpiexec -n $procs_1 ./allreduce $h2_5

mpiexec -n $procs_1 ./allreduce_ring_async $h2_6
mpiexec -n $procs_1 ./allreduce_ring $h2_6
mpiexec -n $procs_1 ./allreduce $h2_6

mpiexec -n $procs_1 ./allreduce_ring_async $h2_7
mpiexec -n $procs_1 ./allreduce_ring $h2_7
mpiexec -n $procs_1 ./allreduce $h2_7

mpiexec -n $procs_1 ./allreduce_ring_async $h2_8
mpiexec -n $procs_1 ./allreduce_ring $h2_8
mpiexec -n $procs_1 ./allreduce $h2_8

mpiexec -n $procs_1 ./allreduce_ring_async $h2_9
mpiexec -n $procs_1 ./allreduce_ring $h2_9
mpiexec -n $procs_1 ./allreduce $h2_9

mpiexec -n $procs_1 ./allreduce_ring_async $h2_10
mpiexec -n $procs_1 ./allreduce_ring $h2_10
mpiexec -n $procs_1 ./allreduce $h2_10


mpiexec -n $procs_2 ./allreduce_ring_async $h2_1
mpiexec -n $procs_2 ./allreduce_ring $h2_1
mpiexec -n $procs_2 ./allreduce $h2_1

mpiexec -n $procs_2 ./allreduce_ring_async $h2_2
mpiexec -n $procs_2 ./allreduce_ring $h2_2
mpiexec -n $procs_2 ./allreduce $h2_2

mpiexec -n $procs_2 ./allreduce_ring_async $h2_3
mpiexec -n $procs_2 ./allreduce_ring $h2_3
mpiexec -n $procs_2 ./allreduce $h2_3

mpiexec -n $procs_2 ./allreduce_ring_async $h2_4
mpiexec -n $procs_2 ./allreduce_ring $h2_4
mpiexec -n $procs_2 ./allreduce $h2_4

mpiexec -n $procs_2 ./allreduce_ring_async $h2_5
mpiexec -n $procs_2 ./allreduce_ring $h2_5
mpiexec -n $procs_2 ./allreduce $h2_5

mpiexec -n $procs_2 ./allreduce_ring_async $h2_6
mpiexec -n $procs_2 ./allreduce_ring $h2_6
mpiexec -n $procs_2 ./allreduce $h2_6

mpiexec -n $procs_2 ./allreduce_ring_async $h2_7
mpiexec -n $procs_2 ./allreduce_ring $h2_7
mpiexec -n $procs_2 ./allreduce $h2_7

mpiexec -n $procs_2 ./allreduce_ring_async $h2_8
mpiexec -n $procs_2 ./allreduce_ring $h2_8
mpiexec -n $procs_2 ./allreduce $h2_8

mpiexec -n $procs_2 ./allreduce_ring_async $h2_9
mpiexec -n $procs_2 ./allreduce_ring $h2_9
mpiexec -n $procs_2 ./allreduce $h2_9

mpiexec -n $procs_2 ./allreduce_ring_async $h2_10
mpiexec -n $procs_2 ./allreduce_ring $h2_10
mpiexec -n $procs_2 ./allreduce $h2_10


mpiexec -n $procs_3 ./allreduce_ring_async $h2_1
mpiexec -n $procs_3 ./allreduce_ring $h2_1
mpiexec -n $procs_3 ./allreduce $h2_1

mpiexec -n $procs_3 ./allreduce_ring_async $h2_2
mpiexec -n $procs_3 ./allreduce_ring $h2_2
mpiexec -n $procs_3 ./allreduce $h2_2

mpiexec -n $procs_3 ./allreduce_ring_async $h2_3
mpiexec -n $procs_3 ./allreduce_ring $h2_3
mpiexec -n $procs_3 ./allreduce $h2_3

mpiexec -n $procs_3 ./allreduce_ring_async $h2_4
mpiexec -n $procs_3 ./allreduce_ring $h2_4
mpiexec -n $procs_3 ./allreduce $h2_4

mpiexec -n $procs_3 ./allreduce_ring_async $h2_5
mpiexec -n $procs_3 ./allreduce_ring $h2_5
mpiexec -n $procs_3 ./allreduce $h2_5

mpiexec -n $procs_3 ./allreduce_ring_async $h2_6
mpiexec -n $procs_3 ./allreduce_ring $h2_6
mpiexec -n $procs_3 ./allreduce $h2_6

mpiexec -n $procs_3 ./allreduce_ring_async $h2_7
mpiexec -n $procs_3 ./allreduce_ring $h2_7
mpiexec -n $procs_3 ./allreduce $h2_7

mpiexec -n $procs_3 ./allreduce_ring_async $h2_8
mpiexec -n $procs_3 ./allreduce_ring $h2_8
mpiexec -n $procs_3 ./allreduce $h2_8

mpiexec -n $procs_3 ./allreduce_ring_async $h2_9
mpiexec -n $procs_3 ./allreduce_ring $h2_9
mpiexec -n $procs_3 ./allreduce $h2_9

mpiexec -n $procs_3 ./allreduce_ring_async $h2_10
mpiexec -n $procs_3 ./allreduce_ring $h2_10
mpiexec -n $procs_3 ./allreduce $h2_10


mpiexec -n $procs_4 ./allreduce_ring_async $h2_1
mpiexec -n $procs_4 ./allreduce_ring $h2_1
mpiexec -n $procs_4 ./allreduce $h2_1

mpiexec -n $procs_4 ./allreduce_ring_async $h2_2
mpiexec -n $procs_4 ./allreduce_ring $h2_2
mpiexec -n $procs_4 ./allreduce $h2_2

mpiexec -n $procs_4 ./allreduce_ring_async $h2_3
mpiexec -n $procs_4 ./allreduce_ring $h2_3
mpiexec -n $procs_4 ./allreduce $h2_3

mpiexec -n $procs_4 ./allreduce_ring_async $h2_4
mpiexec -n $procs_4 ./allreduce_ring $h2_4
mpiexec -n $procs_4 ./allreduce $h2_4

mpiexec -n $procs_4 ./allreduce_ring_async $h2_5
mpiexec -n $procs_4 ./allreduce_ring $h2_5
mpiexec -n $procs_4 ./allreduce $h2_5

mpiexec -n $procs_4 ./allreduce_ring_async $h2_6
mpiexec -n $procs_4 ./allreduce_ring $h2_6
mpiexec -n $procs_4 ./allreduce $h2_6

mpiexec -n $procs_4 ./allreduce_ring_async $h2_7
mpiexec -n $procs_4 ./allreduce_ring $h2_7
mpiexec -n $procs_4 ./allreduce $h2_7

mpiexec -n $procs_4 ./allreduce_ring_async $h2_8
mpiexec -n $procs_4 ./allreduce_ring $h2_8
mpiexec -n $procs_4 ./allreduce $h2_8

mpiexec -n $procs_4 ./allreduce_ring_async $h2_9
mpiexec -n $procs_4 ./allreduce_ring $h2_9
mpiexec -n $procs_4 ./allreduce $h2_9

mpiexec -n $procs_4 ./allreduce_ring_async $h2_10
mpiexec -n $procs_4 ./allreduce_ring $h2_10
mpiexec -n $procs_4 ./allreduce $h2_10

mpiexec -n $procs_5 ./allreduce_ring_async $h2_1
mpiexec -n $procs_5 ./allreduce_ring $h2_1
mpiexec -n $procs_5 ./allreduce $h2_1

mpiexec -n $procs_5 ./allreduce_ring_async $h2_2
mpiexec -n $procs_5 ./allreduce_ring $h2_2
mpiexec -n $procs_5 ./allreduce $h2_2

mpiexec -n $procs_5 ./allreduce_ring_async $h2_3
mpiexec -n $procs_5 ./allreduce_ring $h2_3
mpiexec -n $procs_5 ./allreduce $h2_3 

mpiexec -n $procs_5 ./allreduce_ring_async $h2_4
mpiexec -n $procs_5 ./allreduce_ring $h2_4
mpiexec -n $procs_5 ./allreduce $h2_4

mpiexec -n $procs_5 ./allreduce_ring_async $h2_5
mpiexec -n $procs_5 ./allreduce_ring $h2_5
mpiexec -n $procs_5 ./allreduce $h2_5

mpiexec -n $procs_5 ./allreduce_ring_async $h2_6
mpiexec -n $procs_5 ./allreduce_ring $h2_6
mpiexec -n $procs_5 ./allreduce $h2_6

mpiexec -n $procs_5 ./allreduce_ring_async $h2_7
mpiexec -n $procs_5 ./allreduce_ring $h2_7
mpiexec -n $procs_5 ./allreduce $h2_7

mpiexec -n $procs_5 ./allreduce_ring_async $h2_8
mpiexec -n $procs_5 ./allreduce_ring $h2_8
mpiexec -n $procs_5 ./allreduce $h2_8

mpiexec -n $procs_5 ./allreduce_ring_async $h2_9
mpiexec -n $procs_5 ./allreduce_ring $h2_9
mpiexec -n $procs_5 ./allreduce $h2_9

mpiexec -n $procs_5 ./allreduce_ring_async $h2_10
mpiexec -n $procs_5 ./allreduce_ring $h2_10
mpiexec -n $procs_5 ./allreduce $h2_10


mpiexec -n $procs_6 ./allreduce_ring_async $h2_1
mpiexec -n $procs_6 ./allreduce_ring $h2_1
mpiexec -n $procs_6 ./allreduce $h2_1

mpiexec -n $procs_6 ./allreduce_ring_async $h2_2
mpiexec -n $procs_6 ./allreduce_ring $h2_2
mpiexec -n $procs_6 ./allreduce $h2_2

mpiexec -n $procs_6 ./allreduce_ring_async $h2_3
mpiexec -n $procs_6 ./allreduce_ring $h2_3
mpiexec -n $procs_6 ./allreduce $h2_3 

mpiexec -n $procs_6 ./allreduce_ring_async $h2_4
mpiexec -n $procs_6 ./allreduce_ring $h2_4
mpiexec -n $procs_6 ./allreduce $h2_4

mpiexec -n $procs_6 ./allreduce_ring_async $h2_5
mpiexec -n $procs_6 ./allreduce_ring $h2_5
mpiexec -n $procs_6 ./allreduce $h2_5

mpiexec -n $procs_6 ./allreduce_ring_async $h2_6
mpiexec -n $procs_6 ./allreduce_ring $h2_6
mpiexec -n $procs_6 ./allreduce $h2_6

mpiexec -n $procs_6 ./allreduce_ring_async $h2_7
mpiexec -n $procs_6 ./allreduce_ring $h2_7
mpiexec -n $procs_6 ./allreduce $h2_7

mpiexec -n $procs_6 ./allreduce_ring_async $h2_8
mpiexec -n $procs_6 ./allreduce_ring $h2_8
mpiexec -n $procs_6 ./allreduce $h2_8

mpiexec -n $procs_6 ./allreduce_ring_async $h2_9
mpiexec -n $procs_6 ./allreduce_ring $h2_9
mpiexec -n $procs_6 ./allreduce $h2_9

mpiexec -n $procs_6 ./allreduce_ring_async $h2_10
mpiexec -n $procs_6 ./allreduce_ring $h2_10
mpiexec -n $procs_6 ./allreduce $h2_10


mpiexec -n $procs_7 ./allreduce_ring_async $h2_1
mpiexec -n $procs_7 ./allreduce_ring $h2_1
mpiexec -n $procs_7 ./allreduce $h2_1

mpiexec -n $procs_7 ./allreduce_ring_async $h2_2
mpiexec -n $procs_7 ./allreduce_ring $h2_2
mpiexec -n $procs_7 ./allreduce $h2_2

mpiexec -n $procs_7 ./allreduce_ring_async $h2_3
mpiexec -n $procs_7 ./allreduce_ring $h2_3
mpiexec -n $procs_7 ./allreduce $h2_3 

mpiexec -n $procs_7 ./allreduce_ring_async $h2_4
mpiexec -n $procs_7 ./allreduce_ring $h2_4
mpiexec -n $procs_7 ./allreduce $h2_4

mpiexec -n $procs_7 ./allreduce_ring_async $h2_5
mpiexec -n $procs_7 ./allreduce_ring $h2_5
mpiexec -n $procs_7 ./allreduce $h2_5

mpiexec -n $procs_7 ./allreduce_ring_async $h2_6
mpiexec -n $procs_7 ./allreduce_ring $h2_6
mpiexec -n $procs_7 ./allreduce $h2_6

mpiexec -n $procs_7 ./allreduce_ring_async $h2_7
mpiexec -n $procs_7 ./allreduce_ring $h2_7
mpiexec -n $procs_7 ./allreduce $h2_7

mpiexec -n $procs_7 ./allreduce_ring_async $h2_8
mpiexec -n $procs_7 ./allreduce_ring $h2_8
mpiexec -n $procs_7 ./allreduce $h2_8

mpiexec -n $procs_7 ./allreduce_ring_async $h2_9
mpiexec -n $procs_7 ./allreduce_ring $h2_9
mpiexec -n $procs_7 ./allreduce $h2_9

mpiexec -n $procs_7 ./allreduce_ring_async $h2_10
mpiexec -n $procs_7 ./allreduce_ring $h2_10
mpiexec -n $procs_7 ./allreduce $h2_10





