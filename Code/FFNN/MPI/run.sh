#!/bin/bash
# Clear the screen
clear

procs_1=128
procs_2=64
procs_3=32
procs_4=16
procs_5=12
procs_6=8
procs_7=4
procs_8=2
procs_9=1

h2_1=10 
h2_2=20
h2_3=30

##########################################
##########################################

mpiexec -n $procs_1 ./allreduce_ring_async $h2_1
mpiexec -n $procs_1 ./allreduce_ring $h2_1
mpiexec -n $procs_1 ./allreduce $h2_1

mpiexec -n $procs_1 ./allreduce_ring_async $h2_2
mpiexec -n $procs_1 ./allreduce_ring $h2_2
mpiexec -n $procs_1 ./allreduce $h2_2

mpiexec -n $procs_1 ./allreduce_ring_async $h2_3
mpiexec -n $procs_1 ./allreduce_ring $h2_3
mpiexec -n $procs_1 ./allreduce $h2_3

##########################################

mpiexec -n $procs_2 ./allreduce_ring_async $h2_1
mpiexec -n $procs_2 ./allreduce_ring $h2_1
mpiexec -n $procs_2 ./allreduce $h2_1

mpiexec -n $procs_2 ./allreduce_ring_async $h2_2
mpiexec -n $procs_2 ./allreduce_ring $h2_2
mpiexec -n $procs_2 ./allreduce $h2_2

mpiexec -n $procs_2 ./allreduce_ring_async $h2_3
mpiexec -n $procs_2 ./allreduce_ring $h2_3
mpiexec -n $procs_2 ./allreduce $h2_3 

##########################################

mpiexec -n $procs_3 ./allreduce_ring_async $h2_1
mpiexec -n $procs_3 ./allreduce_ring $h2_1
mpiexec -n $procs_3 ./allreduce $h2_1

mpiexec -n $procs_3 ./allreduce_ring_async $h2_2
mpiexec -n $procs_3 ./allreduce_ring $h2_2
mpiexec -n $procs_3 ./allreduce $h2_2

mpiexec -n $procs_3 ./allreduce_ring_async $h2_3
mpiexec -n $procs_3 ./allreduce_ring $h2_3
mpiexec -n $procs_3 ./allreduce $h2_3 

##########################################

mpiexec -n $procs_4 ./allreduce_ring_async $h2_1
mpiexec -n $procs_4 ./allreduce_ring $h2_1
mpiexec -n $procs_4 ./allreduce $h2_1

mpiexec -n $procs_4 ./allreduce_ring_async $h2_2
mpiexec -n $procs_4 ./allreduce_ring $h2_2
mpiexec -n $procs_4 ./allreduce $h2_2

mpiexec -n $procs_4 ./allreduce_ring_async $h2_3
mpiexec -n $procs_4 ./allreduce_ring $h2_3
mpiexec -n $procs_4 ./allreduce $h2_3 

##########################################

mpiexec -n $procs_5 ./allreduce_ring_async $h2_1
mpiexec -n $procs_5 ./allreduce_ring $h2_1
mpiexec -n $procs_5 ./allreduce $h2_1

mpiexec -n $procs_5 ./allreduce_ring_async $h2_2
mpiexec -n $procs_5 ./allreduce_ring $h2_2
mpiexec -n $procs_5 ./allreduce $h2_2

mpiexec -n $procs_5 ./allreduce_ring_async $h2_3
mpiexec -n $procs_5 ./allreduce_ring $h2_3
mpiexec -n $procs_5 ./allreduce $h2_3 

##########################################

mpiexec -n $procs_6 ./allreduce_ring_async $h2_1
mpiexec -n $procs_6 ./allreduce_ring $h2_1
mpiexec -n $procs_6 ./allreduce $h2_1

mpiexec -n $procs_6 ./allreduce_ring_async $h2_2
mpiexec -n $procs_6 ./allreduce_ring $h2_2
mpiexec -n $procs_6 ./allreduce $h2_2

mpiexec -n $procs_6 ./allreduce_ring_async $h2_3
mpiexec -n $procs_6 ./allreduce_ring $h2_3
mpiexec -n $procs_6 ./allreduce $h2_3 
##########################################

mpiexec -n $procs_7 ./allreduce_ring_async $h2_1
mpiexec -n $procs_7 ./allreduce_ring $h2_1
mpiexec -n $procs_7 ./allreduce $h2_1

mpiexec -n $procs_7 ./allreduce_ring_async $h2_2
mpiexec -n $procs_7 ./allreduce_ring $h2_2
mpiexec -n $procs_7 ./allreduce $h2_2

mpiexec -n $procs_7 ./allreduce_ring_async $h2_3
mpiexec -n $procs_7 ./allreduce_ring $h2_3
mpiexec -n $procs_7 ./allreduce $h2_3 
##########################################

mpiexec -n $procs_8 ./allreduce_ring_async $h2_1
mpiexec -n $procs_8 ./allreduce_ring $h2_1
mpiexec -n $procs_8 ./allreduce $h2_1

mpiexec -n $procs_8 ./allreduce_ring_async $h2_2
mpiexec -n $procs_8 ./allreduce_ring $h2_2
mpiexec -n $procs_8 ./allreduce $h2_2

mpiexec -n $procs_8 ./allreduce_ring_async $h2_3
mpiexec -n $procs_8 ./allreduce_ring $h2_3
mpiexec -n $procs_8 ./allreduce $h2_3 
##########################################

mpiexec -n $procs_9 ./allreduce_ring_async $h2_1
mpiexec -n $procs_9 ./allreduce_ring $h2_1
mpiexec -n $procs_9 ./allreduce $h2_1

mpiexec -n $procs_9 ./allreduce_ring_async $h2_2
mpiexec -n $procs_9 ./allreduce_ring $h2_2
mpiexec -n $procs_9 ./allreduce $h2_2

mpiexec -n $procs_9 ./allreduce_ring_async $h2_3
mpiexec -n $procs_9 ./allreduce_ring $h2_3
mpiexec -n $procs_9 ./allreduce $h2_3 





